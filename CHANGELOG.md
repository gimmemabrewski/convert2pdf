# md-html2pdf Changelog

### 0.1.0

- Initial release

### 0.1.2

- Added another mimetype to md-html2pdf.desktop

### 0.2.0

- Complete overhaul

- Removed the separate desktop entries
    - everything functions through md-html2pdf script
    - removed the option to convert to a csv

- Added a bunch of css files
    - Thanks to all those who have provided them :)

- Added options to choose from built in css or providing your own

- Unless `--css` option is set to 'none', this attempts to override all page themes
    - html's is parsed in an attempt to comment out any stylesheet references
    - this is not the case when providing a website url unless `--force-css` is used

- Added quite a few help screens when using the cli

- All built in styles can be chosen in the right click action menu

- Attempted to sort all the mimetypes this should work for
    - Much testing still needed

- Added configuration options
    - a template can be copied to `$HOME/.convert2pdf/convert2pdf.config` using `md-html2pdf --config-template`
    - this allows for setting the default stylesheet (or none) and a default output path
    - run `--config-template` through the CLI to get the file

- Completely reorganized the project directories

- README is still minimal but you can view the cli options
    - moved it to the project root so maybe gitlab can find it....

### 0.2.1

- Minor changes with mime-types

- Fixed spaces in desktop entry that were causing problems

- added a dependency `psiconv` because ssconvert complained

### 0.2.2

- fixed typos/screw ups
