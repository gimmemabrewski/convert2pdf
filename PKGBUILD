# Maintainer: gimmemabrewski <beebeapps_debugging@tuta.io>

pkgname=convert2pdf
pkgdesc="Create a pdf from a variety of file formats"
pkgver=0.2.2
pkgrel=1
arch=('any')
license=('MIT')
depends=('chromium' 'pandoc' 'zsh' 'gnumeric' 'psiconv')
optdepends=('dolphin: right click action support')
install="$pkgname.install"
url="https://gitlab.com/gimmemabrewski/convert2pdf"
sha256sums=('9b5c86f1398621f87f65e8b0a07929eb4d62a525053f5c9857fae698dd4fe793')

source=( "$pkgname-$pkgver.tar.gz::https://gitlab.com/gimmemabrewski/convert2pdf/-/archive/master/convert2pdf-master.tar.gz" )

changelog="CHANGELOG.md"
readme="README.md"

prepare () {
    cd "$srcdir/${pkgname}-master"
    sed -i "s^VERSION=^VERSION=$pkgver^" "$PWD/src/bin/md-html2pdf.zsh"
}

package () {
    cd "$srcdir/${pkgname}-master"
    printf '%s\n' "$(ls $PWD)"
    mkdir -p ${pkgdir}/{usr/{bin,share/{icons/hicolor/scalable/apps,kio/servicemenus}},opt/convert2pdf/{functions,css,docs}}

    # execs
    install -m 755 "src/bin/md-html2pdf.zsh"      "${pkgdir}/usr/bin/md-html2pdf"
    install -m 755 "src/bin/md-html2pdf.desktop"  "${pkgdir}/usr/share/kio/servicemenus/md-html2pdf.desktop"

    # app functions
    install -m 644 "src/functions/builtin-css"          "${pkgdir}/opt/convert2pdf/functions/builtin-css"
    install -m 644 "src/functions/chk-mimetype"         "${pkgdir}/opt/convert2pdf/functions/chk-mimetype"
    install -m 644 "src/functions/chk-perms"            "${pkgdir}/opt/convert2pdf/functions/chk-perms"
    install -m 644 "src/functions/count-br"             "${pkgdir}/opt/convert2pdf/functions/count-br"
    install -m 644 "src/functions/err-chrome"           "${pkgdir}/opt/convert2pdf/functions/err-chrome"
    install -m 644 "src/functions/find-html-start-pos"  "${pkgdir}/opt/convert2pdf/functions/find-html-start-pos"
    install -m 644 "src/functions/find-tag-index"       "${pkgdir}/opt/convert2pdf/functions/find-tag-index"
    install -m 644 "src/functions/get-outpath"          "${pkgdir}/opt/convert2pdf/functions/get-outpath"
    install -m 644 "src/functions/help-msg"             "${pkgdir}/opt/convert2pdf/functions/help-msg"
    install -m 644 "src/functions/list-builtin-css"     "${pkgdir}/opt/convert2pdf/functions/list-builtin-css"
    install -m 644 "src/functions/parse-ss"             "${pkgdir}/opt/convert2pdf/functions/parse-ss"
    install -m 644 "src/functions/trim"                 "${pkgdir}/opt/convert2pdf/functions/trim"
    install -m 644 "src/functions/unique"               "${pkgdir}/opt/convert2pdf/functions/unique"
    install -m 644 "src/functions/url-css"              "${pkgdir}/opt/convert2pdf/functions/url-css"
    install -m 644 "src/functions/url-html"             "${pkgdir}/opt/convert2pdf/functions/url-html"
    install -m 644 "src/functions/view-supported-types" "${pkgdir}/opt/convert2pdf/functions/view-supported-types"

    # icons
    install -m 644 "src/icons/convert2pdf-dark.svg"  "${pkgdir}/usr/share/icons/hicolor/scalable/apps/convert2pdf-dark.svg"
    install -m 644 "src/icons/convert2pdf-light.svg" "${pkgdir}/usr/share/icons/hicolor/scalable/apps/convert2pdf-light.svg"
    install -m 644 "src/icons/convert2pdf-app.svg"   "${pkgdir}/usr/share/icons/hicolor/scalable/apps/convert2pdf-app.svg"
    install -m 644 "src/icons/convert2pdf-clear.svg" "${pkgdir}/usr/share/icons/hicolor/scalable/apps/convert2pdf-clear.svg"

    # docs
    install -m 644 "README.md"              "${pkgdir}/opt/convert2pdf/docs/README.md"
    install -m 644 "CHANGELOG.md"           "${pkgdir}/opt/convert2pdf/docs/CHANGELOG.md"
    install -m 644 "src/supported_printout" "${pkgdir}/opt/convert2pdf/supported_printout"

    # config
    install -m 644 "src/convert2pdf.config" "${pkgdir}/opt/convert2pdf/convert2pdf.config"

    # css stylesheets
    install -m 644 "src/css/air.css"                  "${pkgdir}/opt/convert2pdf/css/air.css"
    install -m 644 "src/css/avenir-white.css"         "${pkgdir}/opt/convert2pdf/css/avenir-white.css"
    install -m 644 "src/css/beamer.css"               "${pkgdir}/opt/convert2pdf/css/beamer.css"
    install -m 644 "src/css/clearness.css"            "${pkgdir}/opt/convert2pdf/css/clearness.css"
    install -m 644 "src/css/clearness-dark.css"       "${pkgdir}/opt/convert2pdf/css/clearness-dark.css"
    install -m 644 "src/css/foghorn.css"              "${pkgdir}/opt/convert2pdf/css/foghorn.css"
    install -m 644 "src/css/github.css"               "${pkgdir}/opt/convert2pdf/css/github.css"
    install -m 644 "src/css/github1.css"              "${pkgdir}/opt/convert2pdf/css/github1.css"
    install -m 644 "src/css/github2.css"              "${pkgdir}/opt/convert2pdf/css/github2.css"
    install -m 644 "src/css/github-dark.css"          "${pkgdir}/opt/convert2pdf/css/github-dark.css"
    install -m 644 "src/css/markdown.css"             "${pkgdir}/opt/convert2pdf/css/markdown.css"
    install -m 644 "src/css/modest.css"               "${pkgdir}/opt/convert2pdf/css/modest.css"
    install -m 644 "src/css/pandoc.css"               "${pkgdir}/opt/convert2pdf/css/pandoc.css"
    install -m 644 "src/css/pubcss-acm-sigchi.css"    "${pkgdir}/opt/convert2pdf/css/pubcss-acm-sigchi.css"
    install -m 644 "src/css/pubcss-acm-sigchi-ea.css" "${pkgdir}/opt/convert2pdf/css/pubcss-acm-sigchi-ea.css"
    install -m 644 "src/css/retro.css"                "${pkgdir}/opt/convert2pdf/css/retro.css"
    install -m 644 "src/css/scidown.css"              "${pkgdir}/opt/convert2pdf/css/scidown.css"
    install -m 644 "src/css/scidown-article.css"      "${pkgdir}/opt/convert2pdf/css/scidown-article.css"
    install -m 644 "src/css/scidown-report.css"       "${pkgdir}/opt/convert2pdf/css/scidown-report.css"
    install -m 644 "src/css/screen.css"               "${pkgdir}/opt/convert2pdf/css/screen.css"
    install -m 644 "src/css/solarized-dark.css"       "${pkgdir}/opt/convert2pdf/css/solarized-dark.css"
    install -m 644 "src/css/solarized-light.css"      "${pkgdir}/opt/convert2pdf/css/solarized-light.css"
    install -m 644 "src/css/screen-light.css"         "${pkgdir}/opt/convert2pdf/css/screen-light.css"
    install -m 644 "src/css/splendor.css"             "${pkgdir}/opt/convert2pdf/css/splendor.css"
    install -m 644 "src/css/swiss.css"                "${pkgdir}/opt/convert2pdf/css/swiss.css"
    install -m 644 "src/css/tufte.css"                "${pkgdir}/opt/convert2pdf/css/tufte.css"
}
