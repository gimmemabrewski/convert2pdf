# Convert2PDF

> Provides a script `md-html2pdf` for converting html (or URL path), latex, markdown, docx, and more to a pdf document using a combination of pandoc, ssconvert, and chromium headless.

> By default, github's css file is provided and used for styling. A different css file can optionally be used and when running `md-html2pdf` from the command line you can provide your own.

> Also included are right click options for KDE's dolphin - 'Export to PDF' with `md-html2pdf`

### md-html2pdf CLI

```console
$ md-html2pdf --options ["/path/to/infile"||"https://html/url/path"] [outfile-name]
```

```console
    Convert From:
          html urls          ( https://* | http://* | ftp://* )
          html files         ( *.html )
          markdown documents ( *.md | *.markdown )
          latex documents    ( *.tex )
          Word documents     ( *.docx )
          Excel documents    ( *.xlsx )
             and more...
    md-html2pdf [OPTIONS] [INPUT] [OUTPUT]

        Options:
          ( -c | --css             )  - Apply local css file to document
                                         - defaults to github's markdown stylesheet
                                         - set to 'none' if no stylesheet is preferred
          ( -d | --dry-run         )  - Print input path and output path without creating any files
          ( -F | --force-css       )  - Force insert a stylesheet when converting a url to a pdf
                                         - by default, css is ignored when printing a url to pdf
          ( -h | --help            )  - This help message
          ( -i | --input           )  - Input file
                                         - can be specified using the first non option argument
          ( -o | --output          )  - Output file
                                         - can be specified using the second non option argument
                                         - saves in current directory if not specified
          ( -v | --version         )  - show version number
          (    | --config-template )  - add configuration template to config directory
                                         - located at '$HOME/.config/convert2pdf.config'
          (    | --debugging       )  - turn on debugging messages
          (    | --list-css        )  - list all builtin css options
          (    | --list-supported  )  - list supported mimetypes and extensions

```
