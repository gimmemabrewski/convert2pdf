#!/usr/bin/zsh

VERSION=

# _APP_HOME="$HOME/Git/convert2pdf/src"          # testing
_APP_HOME="/opt/convert2pdf"

_ADDED_TEMPLATE=''
CONFIG_LOCATION="$HOME/.config/convert2pdf"
CSS_FILE=''
DRY_RUN=''
FORCE_CSS=false
_URL_INPUT=false
INPUT_FILE=""
OUTPUT_FILE=""
OUTPUT_FOLDER=''
_BUILTIN_CSS=( air avenir-white beamer clearness clearness-dark foghorn modest github github1
               github2 github-dark markdown pandoc pubcss-acm-sigchi pubcss-acm-sigchi-ea retro
               scidown scidown-article scidown-report screen screen-light solarized-dark solarized-light
               splendor swiss tufte )

DEBUGGING_MODE=''
for arg in $@; do if [[ $arg == --debugging ]]; then DEBUGGING_MODE=1; break; fi; done

err () { >&2 printf '%b\n' "\033[1;31m  [ERROR]\033[0;3m $1\033[0m" }
warn () { >&2 printf '%b\n' "\033[1;33m  [WARNING]\033[0;3m $1\033[0m" }
debug () { if [[ -n $DEBUGGING_MODE ]]; then >&2 printf '%b\n' "\033[1m  [DEBUG]\033[0;2;37;3m $1\033[0m"; fi }

_FUNCTIONS=( builtin-css
             chk-mimetype
             chk-perms
             count-br
             err-chrome
             find-html-start-pos
             find-tag-index
             get-outpath
             help-msg
             list-builtin-css
             parse-ss
             trim
             unique
             url-css
             url-html
             view-supported-types
             )

fpath=( "$_APP_HOME/functions" "${fpath[@]}" )
for i in $_FUNCTIONS; do autoload -Uz "$fpath[1]/$i"; done

if [[ $(chk-perms "$PWD") == true ]]; then
    _DEFAULT_OUT="$PWD"; debug "Set fallback output location to $PWD"
else
    _DEFAULT_OUT=$HOME; debug "Set fallback output location to $HOME"
fi

if [[ -f "${CONFIG_LOCATION}/convert2pdf.config" ]]; then
    for line in "${(@f)$(<"${CONFIG_LOCATION}/convert2pdf.config")}"; do
        if [[ $line =~ ' *css_stylesheet *= *+.+' ]]; then
            debug "Found css_stylesheet option in user config"
            CSS_FILE="$(trim ${${(s:=:)line}[2]})"
        elif [[ $line =~ ' *output_directory *= *+.+' ]]; then
            debug "Found output_directory option in user config"
            OUTPUT_FOLDER="$(trim ${${(s:=:)line}[2]})"
        fi
    done
fi

if [[ -z $OUTPUT_FOLDER ]]; then
    OUTPUT_FOLDER="$_DEFAULT_OUT"
elif [[ ! -d $OUTPUT_FOLDER ]]; then
    if [[ -f $OUTPUT_FOLDER ]]; then
        warn "Invalid output location - '$OUTPUT_FOLDER' is not a directory!"
    else
        warn "Invalid directory - '$OUTPUT_FOLDER' doesn't exist!"
    fi
    OUTPUT_FOLDER="$_DEFAULT_OUT"
elif [[ $(chk-perms "$OUTPUT_FOLDER") == false ]]; then
    warn "Permission error - can't write to '$OUTPUT_FOLDER'"
    OUTPUT_FOLDER="$_DEFAULT_OUT"
fi

while [[ -n $1 ]]; do
    case $1 in
    -d|--dry-run) DRY_RUN=1 ;;
    -c|--css) shift; CSS_FILE="$1" ;;
    -F|--force-css) FORCE_CSS=true ;;
    -h|--help) help-msg; exit 0 ;;
    -i|--input) shift
        if [[ -n $INPUT_FILE ]]; then err "Only one input file can be specified!"; exit 1; fi
        INPUT_FILE="$1"
        ;;
    -o|--output) shift
        if [[ -n $OUTPUT_FILE ]]; then err "Only one output file can be specified!"; exit 1; fi
        OUTPUT_FILE="$(get_outpath "$1")"; if [[ $? -gt 0 ]]; then exit 1; fi
        ;;
    -v|--version)
        printf '%b\n' "\n  \033[1;37;4mmd-html2pdf\033[0m" \
            "\033[0;1;30m    version\033[0;0;37;3m $VERSION\033[0m\n"
        exit 0
        ;;
    --config-template)
        _ADDED_TEMPLATE=1
        mkdir -p "$CONFIG_LOCATION"
        cp "$_APP_HOME/convert2pdf.config" "${CONFIG_LOCATION}/convert2pdf.config"
        ;;
    --debugging) ;;
    --list-css) list-builtin-css; exit 0 ;;
    --list-supported) view-supported-types; exit 0 ;;
    *)
        if [[ -z $INPUT_FILE  && $1 != '-'* ]]; then INPUT_FILE="$1"
        elif [[ -n $INPUT_FILE && -z $OUTPUT_FILE ]]; then
            OUTPUT_FILE="$(get-outpath "$1")"; if [[ $? -gt 0 ]]; then exit 1; fi
        else err "Invalid option '$1'"; help-msg opts; exit 1
        fi
    esac
    shift
done

if [[ -n $_ADDED_TEMPLATE && -z $INPUT_FILE ]]; then exit 0; fi

if [[ -z $CSS_FILE ]]; then CSS_FILE="$(builtin-css github)"; debug "Defaulting to builtin css 'github.css'"
elif [[ $CSS_FILE == none ]]; then CSS_FILE=''; debug "Setting css stylesheet to none"
elif [[ -n $CSS_FILE ]]; then
    if [[ ${_BUILTIN_CSS[(r)$CSS_FILE]} == $CSS_FILE ]]; then
        debug "Applying builtin css '$CSS_FILE.css'"; CSS_FILE="$(builtin-css "$CSS_FILE")"

    elif [[ $CSS_FILE =~ '^[0-9]+$' && $CSS_FILE -ge 1 && $CSS_FILE -le $#_BUILTIN_CSS ]]; then
        CSS_FILE="${${(n)_BUILTIN_CSS}[$CSS_FILE]}"
        debug "Applying builtin css '$CSS_FILE.css'"; CSS_FILE="$(builtin-css "$CSS_FILE")"

    elif [[ $CSS_FILE == (http://*|https://*|ftp://*) ]]; then
        debug "Applying css stylesheet from a web url"; CSS_FILE="$(url-css "$CSS_FILE")"
        if [[ $? -gt 0 ]]; then exit 1; fi

    elif [[ ! -f $CSS_FILE ]]; then err "CSS file '$1' doesn't exist!"; exit 1
    elif [[ $CSS_FILE != *.css  && ${${(s: :)$(file --mime-type "$CSS_FILE")}[2]} != ('text/plain'|'text/css') ]]; then err "Invalid css file!"; exit 1
    fi
fi

_COMMAND='url'
if [[ -z $INPUT_FILE ]]; then err "No input file specified"; exit 1
elif [[ $INPUT_FILE == (https://*|http://*|ftp://*) ]]; then
    debug "Setting _URL_INPUT to true"; _URL_INPUT=true;
    INPUT_FILE="$(url-html "$INPUT_FILE")"; if [[ $? -gt 0 ]]; then exit 1; fi
elif [[ ! -f $INPUT_FILE ]]; then err "File '$1' doesn't exist!"; exit 1
else
    _COMMAND="$(chk-mimetype "$INPUT_FILE")"
    if [[ $? -gt 0 ]]; then exit 1; fi
fi

if [[ -z $OUTPUT_FILE ]]; then
    OUTPUT_FILE="$(unique "$OUTPUT_FOLDER/$(basename "$INPUT_FILE")")"
fi

if [[ -n $DRY_RUN ]]; then
    printf '%b\n' "" "\033[1;37m  Dry run:\033[0;0;36;3m '$INPUT_FILE'" \
        "\033[0;1;33m           >>>" \
        "\033[0;1;36;3m               '$OUTPUT_FILE'\033[0m" "" \
        "\033[0;0;33m      From url:    \033[0;32m $_URL_INPUT\033[0m" \
        "\033[0;0;33m      Force CSS:   \033[0;32m $FORCE_CSS\033[0m" \
        "\033[0;0;33m      CSS:         \033[2;36;3m $CSS_FILE\033[0m" \
        "\033[0;0;33m      Command Type:\033[2;37;3m ${(C)_COMMAND}\033[0m" ""
    exit 0
else
    tmpdir="$(mktemp -p /tmp -d XXXXXXXXXX)"
    errfile="$(mktemp -p $tmpdir XXXXXXXX)"
    tmp="$(mktemp -p $tmpdir XXXXXXXX.html)"
    tmphtml="$(mktemp -p $tmpdir XXXXXXXX.html)"

    if [[ $_COMMAND == ('url'|'html') ]]; then
        cat "$INPUT_FILE" > "$tmp"
    elif [[ $_COMMAND == 'pandoc' ]]; then
        pandoc -t html -o "$tmp" "$INPUT_FILE"
    else
        ssconvert -T Gnumeric_html:html40 "$INPUT_FILE" "$tmp"
    fi

    INPUT_DATA="$(<$tmp)"
    if [[ ${INPUT_DATA:l} == '<!doctype'* ]]; then
        n=1
        while [[ $INPUT_DATA[$n] != '>' ]]; do ((n++)); done
        HEADING="${${INPUT_DATA[1,$n]}//$'\n'/}"
        INPUT_DATA="${INPUT_DATA[$((n+1)),-1]}"
        while [[ $INPUT_DATA == $'\n'* ]]; do debug "removing starting newline"; INPUT_DATA="${INPUT_DATA[2,-1]}"; done

    else
        HEADING="<!DOCTYPE html>"
    fi

    if [[ -z $(printf '%b' "$INPUT_DATA" | awk '/<body/;/<head/') ]]; then
        debug "html missing both <html> and <body> tags"
        _index=$(($(find-html-start-pos "$INPUT_DATA")-1))
        debug "html start pos = '$_index'"
        _input=( "${INPUT_DATA[1,$_index]}" "<html><body>" "${INPUT_DATA[$_index,-1]}" "</body></html>" )
        INPUT_DATA="${(j:\n:)"${_input[@]}"}"

    elif [[ -z $(printf '%b' "$INPUT_DATA" | awk '/<html/') ]]; then
        debug "html missing <html> tags"
        _index=$(($(find-html-start-pos "$INPUT_DATA")-1))
        _input=( "${INPUT_DATA[1,$_index]}" "<html>" "${INPUT_DATA[$_index,-1]}" "</html>" )
        INPUT_DATA="${(j:\n:)"${_input[@]}"}"

    elif [[ -z $(printf '%b' "$INPUT_DATA" | awk '/<body/') ]]; then
        html_start=$(find-tag-index "$INPUT_DATE" '<html[^>]*?>' end)
        debug "html_start index = '$html_start'"
        html_end=$(find-tag-index "$INPUT_DATE" '</html>' )
        debug "html_end index = '$html_end'"
        if [[ -n $html_begin && -n $html_end ]]; then
            _input=( "${INPUT_DATA[1,$html_start]}" "<body>"
                     "${INPUT_DATA[$((html_start+1)),$((html_end-1))]}"
                     "</body>" "${INPUT_DATA[$html_end,-1]}" )
            INPUT_DATA="${(j:\n:)"${_input[@]}"}"
        else
            warn "Found <html> tag but can't find indexes"
        fi
    fi

    if [[ -n $CSS_FILE  && ( $_URL_INPUT == false || $FORCE_CSS == true ) ]]; then
        pat='^<link[^>]*?type=.?text\/css.*?>$'
        _refs=("${(@f)$(printf '%b' "$INPUT_DATA" | awk "/${pat}/")}")
        debug "Found '$#_refs' stylesheet references"

        _input="$INPUT_DATA"
        for ref in $_refs; do
            _input="$(printf '%b' "$_input" | sed "s@$ref@ <\!-- $ref --> @")"
        done

        if [[ -n $_input ]]; then INPUT_DATA="$_input"; else warn "Error removing stylesheet references"; fi

        CSS_DATA="$(parse-ss "$CSS_FILE")"

        if [[ -n $(printf '%b' "$INPUT_DATA" | awk '/<\/style>/') ]]; then
            _input=( "$HEADING" "${"${(@f)$(printf '%b' "$INPUT_DATA" | sed "s@</style>@${CSS_DATA//@/\\@}</style>@")}"[@]}" )
        else
            _input=( "$HEADING" "<style>" "$CSS_DATA" "</style>" "$INPUT_DATA" )
        fi

        if [[ -n $_input ]]; then INPUT_DATA="${(j:\n:)"${_input[@]}"}"; else warn "Error adding stylesheet data to html"; fi

    elif [[ $_URL_INPUT == false && -z $CSS_FILE && -z $(printf '%s' "$INPUT_DATA" | grep '@media print') ]]; then
        pagestyle=( ""
                    "  @media print {"
                    "    @page {"
                    "      margin: 0;"
                    "      printBackground: true;"
                    "    }"
                    "  }"
                    ""
                    )
        if [[ -n $(printf '%b' "$INPUT_DATA" | awk '/<\/style>/') ]]; then
            _input=( "$HEADING" "${"${(@f)$(printf '%b' "$INPUT_DATA" | sed "s@</styles>@${(j:\n:)"${pagestyle[@]}"}</styles>@")}"[@]}" )
        else
            _input=( "$HEADING" "<style>" "${pagestyle[@]}" "</style>" "$INPUT_DATA" )
        fi

        if [[ -n $_input ]]; then INPUT_DATA="${(j:\n:)"${_input[@]}"}"; else warn "Error adding media styles"; fi
    else
        _input=( "$HEADING" "$INPUT_DATA" )
        INPUT_DATA="${(j:\n:)"${_input[@]}"}"
    fi

    printf '%b' "${INPUT_DATA}" > $tmphtml
    pushd "$tmpdir"

    chromium \
        --headless \
        --window-size=1024,768 \
        --enable-features=ConversionMeasurement,AttributionReportingCrossAppWeb \
        --disable-pdf-tagging \
        --no-pdf-header-footer \
        --run-all-compositor-stages-before-draw \
        --print-to-pdf="$PWD/output.pdf" \
        "$tmphtml" 2> $errfile &> /dev/null
    if [[ $? -gt 0 ]]; then err-chrome $errfile; exit 1; fi

    mv "$PWD/output.pdf" $OUTPUT_FILE
    popd

    printf '\n%b\n\n' "\033[1;37m  File written to\033[0;1;36;3m '$OUTPUT_FILE'\033[0m"
    exit 0
fi
